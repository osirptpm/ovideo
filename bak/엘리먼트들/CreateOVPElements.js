/**
 * get videoElements
 * get oVPlayerList
 */
//import "/OVPlayer/css/_oVPCSS.css";

export default class CreateOVPElements {
    constructor(videoElements) {
        this._videoElements = videoElements;
        this._oVPSecList = this.replaceElements();
    }

    get videoElements() {
        return this._videoElements;
    }

    get oVPlayerList() {
        return this._oVPSecList;
    }

    createDom(vidEl) {
        let htmlText = "<section class='_oVPSec'>" +
                vidEl.outerHTML +
                "<section class='_coverSec'>" +
                "</section>" +
            "</section>";
        let domParser = new DOMParser();
        return domParser.parseFromString(htmlText, "text/html").getElementsByClassName("_oVPSec")[0];
    }

    replaceElements() {
        this._oVPSecList = [];
        for (let i = 0; i < this._videoElements.length; i++) {
            let el = this._videoElements[i];
            let paEl = el.parentElement;
            let newEl = this.createDom(el);
            paEl.replaceChild(newEl, el);
            this._oVPSecList[i] = newEl;
        }
        return this._oVPSecList;
    }
}