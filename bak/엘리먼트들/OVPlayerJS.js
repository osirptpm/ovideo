import CreateOVPElements from "/OVPlayer/js/CreateOVPElements.js";
import AddFeatures from "/OVPlayer/js/AddFeatures.js";

export default class OVPlayerJS {
    constructor(videoElements) {
        window.onload = function () {
            OVPlayerJS.loadScriptOrCSS("css", "/OVPlayer/css/_oVPCSS.css");
            new CreateOVPElements(videoElements);
        };
    }

    static loadScriptOrCSS(filetype, filename) {
        let fileref;
        if (filetype === "js") { //if filename is a external JavaScript file
            fileref = document.createElement('script');
            fileref.setAttribute("src", filename);
        }
        else if (filetype === "css"){ //if filename is an external CSS file
            fileref = document.createElement("link");
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("href", filename);
        }
        if (typeof fileref !== "undefined")
            document.getElementsByTagName("head")[0].appendChild(fileref);
    }
}

//OVPlayerJS.loadScriptOrCSS("js", "/OVPlayer/js/CreateOVPElements.js");
//OVPlayerJS.loadScriptOrCSS("js", "/OVPlayer/js/AddFeatures.js");

// todo import css하는법, 마저 기능 개발