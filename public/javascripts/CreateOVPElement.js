/**
 * get videoElements
 * get oVPlayerList
 */
//import "/OVPlayer/css/_oVPCSS.css";

export default class CreateOVPElement {
    constructor(videoElement) {
        //this.__videoElement = videoElement;
        this.__oVPSec = this._replaceElement(videoElement);
    }

    /*get getVideoElement() {
        return this.__videoElement;
    }*/

    get getOVPlayer() {
        return this.__oVPSec;
    }

    _createDom(vidEl) {
        let htmlText =
            "<section class='_oVPSec'>" +
                "<section class='_coverSec'>" +
                "</section>" +
            "</section>";
        let domParser = new DOMParser();
        let oVPSec =  domParser.parseFromString(htmlText, "text/html").body.firstChild;
        oVPSec.insertBefore(vidEl, oVPSec.childNodes[0]);
        return oVPSec;
    }
    _replaceElement(videoElement) {
        let el = videoElement;
        let paEl = el.parentElement;
        let oVPSec = this._createDom(el);
        paEl.appendChild(oVPSec);
        return oVPSec;
    }


    _TESTCreateDom(htmlText) {
        return new DOMParser().parseFromString(htmlText, "text/html").body.firstChild;
    }
    _TESTBuildOVPlayerElement(videoElement) {
        let paEl = videoElement.parentElement;
        let _oVPSec = this._createDom(
            "<section class='_oVPSec'>" +
            "</section>"
        );
        let _coverSec = this._createDom(
            "<section class='_coverSec'>" +
            "</section>"
        );
        _oVPSec.appendChild(videoElement);
        paEl.appendChild(_oVPSec);
        console.log(paEl);
        return _oVPSec;
    }
}