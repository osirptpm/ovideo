
const VTT = "vtt";
const SRT = "srt";
const SMI = "smi";

export default class SubtitlesRegistration {
    constructor(videoElement) {
        this.__videoElement = videoElement;
        this.__compatibilityMode = false;
        this._addSubtitles(videoElement);
    }

    set setCompatibilityMode(boolean) {
        this.__compatibilityMode = boolean;
    }

    set setEventCueChange(func) {
        if (typeof func !== "function") {
            this.__cueChangeFunc = () => {};
            return;
        }
        this.__cueChangeFunc = func;
    }

    _getTrackElements(videoElement) {
        let trackElements = videoElement.getElementsByTagName("track");
        for (let i = 0; i < trackElements.length; i++) {
            console.log(trackElements[i]);
        }
        return trackElements;
    }

    _getAsyncSubtitleFile(url) {
        return new Promise((resolve, reject) => {
            let xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = (e) => {
                if (xhttp.readyState === 4) {
                    if (xhttp.status === 200) {
                        resolve(xhttp.responseText);
                    } else {
                        reject(e);
                    }
                }
            };
            xhttp.open("GET", url);
            xhttp.send(null);
        });
    }

    _addSubtitles(videoElement) {
        let trackElements = this._getTrackElements(videoElement);
        const trackElementsLength = trackElements.length;
        for (let i = 0; i < trackElementsLength; i++) {
            let src = trackElements[i].src;
            let filename = trackElements[i].label;
            let extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length).toLowerCase();
            this._getAsyncSubtitleFile(src).then((text) => {
                //console.log(text);
                // console.log("성공");
                //let textTrack = videoElement.addTextTrack("subtitles", trackElements[i].track.label, trackElements[i].track.language);
                let option = {
                    kind: trackElements[i].track.kind,
                    label: trackElements[i].track.label,
                    language: trackElements[i].track.language,
                    extension: extension
                };
                //textTrack.extension = extension;
                //if (videoElement.textTracks[i].label === "srt") textTrack.mode = "showing";
                //textTrack = "disabled";
                // console.log(trackElements[i].track.mode);
                //this._addCueOnTextTrack(videoElement.textTracks[i], text, extension);
                this._addCueOnTextTrack(videoElement, option, text);
                trackElements[i].remove();
                console.log("자막 등록 성공", videoElement.textTracks);
            }, (error) => {
                console.log("error", error);
            } );
        }
    }

    _addCueOnTextTrack(videoElement, option, text) {
        let textTracks;
        switch(option.extension) {
            case VTT: return;
            case SRT: textTracks = this._srt2vtt(videoElement, option, text); break;
            case SMI: textTracks = this._smi2vtt(videoElement, option, text); break;
            default : console.log("지원하는 확장자가 아닙니다."); return;
        }

        for (let i = 0; i < textTracks.length; i++) {
            this._addEventCueChange(textTracks[i]);
            textTracks[i].mode = "disabled";
        }
    }

    _srt2vtt(videoElement, option, text) {
        let textTrack = videoElement.addTextTrack(option.kind, option.label, option.language);

        const timePattern = /[\r\n]{1,2}(\d\d):(\d\d):(\d\d),(\d\d\d) --> (\d\d):(\d\d):(\d\d),(\d\d\d)[\r\n]{1,2}/g;
        const indexPattern = /[\r\n]{1,2}\d+[\r\n]{1,2}/g;
        let timeMatchArray, indexMatchArray;
        //timeMatchArray = /\r\n\d+\r\n/.exec(text);
        //let test = /[\r\n]{0,2}(\d\d):(\d\d):(\d\d),(\d\d\d) --> (\d\d):(\d\d):(\d\d),(\d\d\d)/.exec(text);
        //console.log(test);
        while((timeMatchArray = timePattern.exec(text)) !== null) {
            let startTime = parseInt(timeMatchArray[1]) * 3600
                + parseInt(timeMatchArray[2]) * 60
                + parseInt(timeMatchArray[3])
                + parseFloat(0 + "." + timeMatchArray[4]);
            let endTime = parseInt(timeMatchArray[5]) * 3600
                + parseInt(timeMatchArray[6]) * 60
                + parseInt(timeMatchArray[7])
                + parseFloat(0 + "." + timeMatchArray[8]);
            let startStr, endStr;
            startStr = timePattern.lastIndex;
            indexPattern.lastIndex = timePattern.lastIndex - 3;
            if ((indexMatchArray = indexPattern.exec(text)) !== null)
                endStr = indexMatchArray.index;
            else endStr = text.length;
            textTrack.addCue(new VTTCue(startTime, endTime, text.substring(startStr, endStr)));
            //console.log(startTime, endTime);
            //console.log(text.substring(startStr, endStr));
        }
        //console.log(textTrack);
        return [textTrack];
    }

    _smi2vtt(videoElement, option, text) {
        const RN = "\r\n", EMPTYSTRING = "", NBSP = "&nbsp;";
        const RNPattern = /[\r\n]{1,2}/g;
        const brPattern = /<[Bb][Rr]\/?>/g;

        const pattern = /<[Ss][Yy][Nn][Cc] [Ss][Tt][Aa][Rr][Tt]=(\d+)/g;
        const pClassPattern =  /<[Pp] +.*\b[Cc][Ll][Aa][Ss][Ss]=(\w+)*.*(?=>)|<[Pp](?=>)/g;

        let matchArray, pClassMatchArray;
        let startIndex, endIndex, startTime, endTime;
        let beforeLang, currentLang;
        let textTracks = [];
        let textTrack;

        while ((matchArray = pattern.exec(text)) !== null) { //todo while 문 정리.. 분해..
            pClassPattern.lastIndex = pattern.lastIndex - 2;
            pClassMatchArray = pClassPattern.exec(text);
            currentLang = pClassMatchArray[1];
            if (beforeLang !== currentLang) {
                textTrack = this._getRightTextTrackFromSmi(videoElement, textTracks, currentLang, option);
                beforeLang = currentLang;
            }

            startTime = parseFloat(matchArray[1]) / 1000;
            startIndex = pClassPattern.lastIndex + 1;

            let startTimeLastIndex = pattern.lastIndex;
            matchArray = pattern.exec(text);

            if (matchArray !== null) {
                endTime = parseFloat(matchArray[1]) / 1000;
                endIndex = matchArray.index;
            } else {
                endTime = startTime + 3;
                endIndex = /<\/[Bb][Oo][Dd][Yy]>/.exec(text).index;
            }

            let sub = text.substring(startIndex, endIndex).replace(RNPattern, EMPTYSTRING).replace(brPattern, RN).replace(NBSP, EMPTYSTRING);
            // if (sub !== "&nbsp;")
            textTrack.addCue(new VTTCue(startTime, endTime, sub));

            pattern.lastIndex = startTimeLastIndex;
        }
        return textTracks;
    }

    _checkLangFromSmi(currentLang, defaultLang) {
        let lang;
        if (/[Kk][Rr]|[Kk][Oo]/.test(currentLang)) lang = "ko";
        else if (/[Ee][Nn]/.test(currentLang)) lang = "en";
        else lang = defaultLang.toUpperCase();
        return lang;
    }
    _newTextTrack(videoElement, textTrackOption) {
        let textTrack = videoElement.addTextTrack(textTrackOption.kind, textTrackOption.label, textTrackOption.lang);
        textTrack.identifier = textTrackOption.identifier;
        return textTrack;
    }
    _getRightTextTrackFromSmi(videoElement, textTracks, currentLang, option) {
        let textTrack = textTracks.find(x => x.identifier === currentLang);
        if (textTrack === undefined) {
            let lang = this._checkLangFromSmi(currentLang, option.language);
            let textTrackOption = {
                kind: option.kind,
                label: option.label + "_" + lang,
                lang: lang,
                identifier: currentLang
            };
            textTrack = this._newTextTrack(videoElement, textTrackOption);
            textTracks.push(textTrack);
        }
        return textTrack;
    }

    _addEventCueChange(textTrack) {
        if (textTrack.oncuechange !== undefined) { // oncuechange이벤트를 지원하는 브라우저일 경우
            textTrack.addEventListener("cuechange", () => {
                this._displayCue(textTrack);
                //console.log(this._videoElement.currentTime);
            });
        }/* else { // oncuechange이벤트를 지원 안하는 브라우저일 경우 timeupdate으로 대체
            this._videoElement.addEventListener("timeupdate", () => {
                this.displayCue(textTrack);
            });
        }*/
    }

    _displayCue(textTrack) {
        let cues = textTrack.activeCues;      // 현재 시간에 맞는 자막 가져오기
        //console.log(textTrack);
        //if (this.__displayElement === undefined || this.__displayElement === null)
        if (typeof this.__cueChangeFunc !== "function") return;
        //this.__displayElement.innerHTML = "";
        for (let i = cues.length - 1; i >= 0  ; i--) {
            this.__cueChangeFunc(cues[i].text.replace(/[\r\n]{1,2}/g, "<br>"));
            //this.__displayElement.innerHTML += "<div>" + cues[i].text.replace(/[\r\n]{1,2}/g, "<br>") + "<div>";
        }
    }
}