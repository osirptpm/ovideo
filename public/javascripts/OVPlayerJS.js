import CreateOVPElement from "./CreateOVPElement.js";
import AddFeatures from "./AddFeatures.js";
import SubtitlesRegistration from "./SubtitlesRegistration.js";


export default class OVPlayerJS {
    constructor(videoElement) {
        //this.__subtitlesRegistration = null;
        //window.onload = function () {
        OVPlayerJS.loadScriptOrCSS("css", "/stylesheets/_oVPCSS.css");
        this.__createOVPElement = new CreateOVPElement(videoElement);
        this.__subtitlesRegistration = new SubtitlesRegistration(videoElement)

        //};
    }
    set videoInfo(json) {

    }

    set setEventCueChange(func) {
        this.__subtitlesRegistration.setEventCueChange = (typeof func !== "function") ? (text) => {
            this.__createOVPElement.getOVPlayer.getElementsByClassName("_coverSec")[0].innerHTML += text;
        } : func;
    }
    static loadScriptOrCSS(filetype, filename) {
        let fileref;
        if (filetype === "js") { //if filename is a external JavaScript file
            fileref = document.createElement('script');
            fileref.setAttribute("src", filename);
        }
        else if (filetype === "css"){ //if filename is an external CSS file
            fileref = document.createElement("link");
            fileref.setAttribute("rel", "stylesheet");
            fileref.setAttribute("href", filename);
        }
        if (typeof fileref !== "undefined")
            document.getElementsByTagName("head")[0].appendChild(fileref);
    }
}

//OVPlayerJS.loadScriptOrCSS("js", "/OVPlayer/js/CreateOVPElement.js");
//OVPlayerJS.loadScriptOrCSS("js", "/OVPlayer/js/AddFeatures.js");

// todo import css하는법, 마저 기능 개발       